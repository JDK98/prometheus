**INSTALL PROMETHEUS WITH HELM** <br>

`helm repo add prometheus-community https://prometheus-community.github.io/helm-charts` <br>
`helm repo update` <br>
`helm install monitoring prometheus-community/kube-prometheus-stack`
